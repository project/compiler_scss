<?php

namespace Drupal\compiler_scss\Type;

/**
 * Defines a Sass number type optionally paired with a unit.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class Number {

  /**
   * The magnitude of the number.
   *
   * @var float
   */
  protected $value = 0.0;

  /**
   * The unit of the number.
   *
   * @var string|null
   */
  protected $unit = NULL;

  /**
   * Constructs a Number object.
   *
   * @param float $value
   *   The magnitude of the number.
   * @param string|null $unit
   *   The unit of the number. Must be supported by the underlying compiler.
   */
  public function __construct(float $value, ?string $unit = NULL) {
    if (isset($unit) && !is_string($unit)) {
      throw new \RuntimeException('$unit must be a string or NULL');
    }

    $this->value = $value;
    $this->unit = $unit;
  }

  /**
   * Get the unit for this number.
   *
   * @return string|null
   *   The unit for this number.
   */
  public function unit(): ?string {
    return $this->unit;
  }

  /**
   * Get the magnitude of this number.
   *
   * @return float
   *   The magnitude of this number.
   */
  public function value(): float {
    return $this->value;
  }

}
