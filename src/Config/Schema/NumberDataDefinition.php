<?php

namespace Drupal\compiler_scss\Config\Schema;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TypedData\ComplexDataDefinitionBase;
use Drupal\Core\TypedData\DataDefinition;

/**
 * A data definition that describes the properties of a Sass number.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class NumberDataDefinition extends ComplexDataDefinitionBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getMainPropertyName() {
    return 'value';
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    if (empty($this->propertyDefinitions)) {
      $definitions['value'] = DataDefinition::create('float')
        ->setLabel($this->t('Float'))
        ->setRequired(TRUE);

      $definitions['unit'] = DataDefinition::create('compiler_scss_unit')
        ->setLabel($this->t('Sass unit'));

      $this->propertyDefinitions = $definitions;
    }

    return $this->propertyDefinitions;
  }

}
