<?php

namespace Drupal\compiler_scss\Config\Schema;

use Drupal\Core\Config\Schema\ArrayElement;

/**
 * A config schema type used to represent a font family sequence.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class FontFamily extends ArrayElement {

  /**
   * {@inheritdoc}
   */
  protected function getElementDefinition($key) {
    $value = $this->value[$key] ?? NULL;

    $definition = [
      'type' => 'string',
    ];

    return $this->buildDataDefinition($definition, $value, $key);
  }

}
