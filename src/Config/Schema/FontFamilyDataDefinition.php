<?php

namespace Drupal\compiler_scss\Config\Schema;

use Drupal\Core\Config\Schema\SequenceDataDefinition;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataDefinitionInterface;

/**
 * A data definition that describes the items in a font family sequence.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class FontFamilyDataDefinition extends SequenceDataDefinition {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values = [], DataDefinitionInterface $item_definition = NULL, ...$args) {
    // Override the item list type to the 'string' data definition.
    parent::__construct($values, DataDefinition::create('string'), ...$args);
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromDataType($type) {
    return self::create($type);
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderBy() {
    return 'key';
  }

}
