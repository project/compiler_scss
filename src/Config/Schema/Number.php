<?php

namespace Drupal\compiler_scss\Config\Schema;

use Drupal\compiler_scss\Type\Number as IntermediateNumber;
use Drupal\Core\Config\Schema\ArrayElement;

/**
 * A config schema type used to represent a Sass number.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class Number extends ArrayElement {

  /**
   * {@inheritdoc}
   */
  protected function getElementDefinition($key) {
    $value = $this->value[$key] ?? NULL;

    /** @var \Drupal\compiler_scss\Config\Schema\NumberDataDefinition */
    $data_definition = $this->getDataDefinition();

    /** @var \Drupal\Core\TypedData\DataDefinition|null */
    $definition = $data_definition->getPropertyDefinition($key);
    $definition = $definition?->toArray() ?? [];

    return $this->buildDataDefinition($definition, $value, $key);
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    $value = $this->toArray() + [
      'value' => NULL,
      'unit' => NULL,
    ];

    if (is_numeric($value['value'])) {
      if (is_string($value['unit']) && strlen($value['unit']) > 0) {
        return new IntermediateNumber($value['value'], $value['unit']);
      }

      return floatval($value['value']);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value, $notify = TRUE) {
    if ($value instanceof IntermediateNumber) {
      $value = [
        'value' => $value->value(),
        'unit' => $value->unit(),
      ];
    }
    elseif (is_numeric($value)) {
      $value = [
        'value' => $value,
      ];
    }

    parent::setValue($value, $notify);
  }

}
