<?php

namespace Drupal\compiler_scss;

use Drupal\compiler\Plugin\CompilerPluginInterface;

/**
 * Provides a common interface for backend implementations.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @internal
 */
interface BackendInterface extends CompilerPluginInterface {

  /**
   * An identifier expression which is a proper subset of both PHP & SCSS.
   *
   * This regular expression should be used to validate function names supplied
   * when registering PHP functions with the compiler.
   *
   * @var string
   */
  const IDENT_EXPR = <<<'REGEX'
  /^[a-zA-Z_][a-zA-Z0-9_]*$/
  REGEX;

  /**
   * Register a PHP callback function bridge that can be used in SCSS code.
   *
   * It is recommended to register all needed functions before the compiler is
   * ran for the first time. If a function is registered after the compiler's
   * first run, then the behavior is undefined.
   *
   * @param callable $cb
   *   The PHP function to call when bridged from SCSS.
   *
   *   Optional and pass-by-reference parameters aren't supported.
   * @param string|null $name
   *   The name of the function as it should be used in the compiled language.
   *   This value must be a valid PHP identifier.
   *
   *   Optional unless an anonymous function is supplied (default: NULL).
   *
   * @throws \InvalidArgumentException
   *   If a bad function name is supplied, or a callback with an optional or
   *   pass-by-reference parameter is supplied.
   */
  public function registerFunction(callable $cb, ?string $name = NULL);

}
