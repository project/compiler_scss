<?php

namespace Drupal\compiler_scss\Plugin\Compiler;

use Drupal\compiler\CompilerContextInterface;
use Drupal\compiler\Plugin\CompilerPluginBase;
use Drupal\compiler_scss\BackendInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The SCSS compiler plugin.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @Compiler("scss")
 */
class ScssCompiler extends CompilerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The backend impementation for this compiler plugin.
   *
   * @var \Drupal\compiler_scss\BackendInterface
   */
  protected $backend;

  /**
   * Constructs an ScssCompiler object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\compiler_scss\BackendInterface $backend
   *   The backend impementation for this compiler plugin.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BackendInterface $backend) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->backend = $backend;
  }

  /**
   * {@inheritdoc}
   */
  public function compile(CompilerContextInterface $context) {
    return $this->__call(__FUNCTION__, func_get_args());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('compiler_scss.backend'),
    );
  }

  /**
   * Capture all method calls and foward them to the backend implementation.
   *
   * @param string $method
   *   The name of the method to forward to the backend implementation.
   * @param array $args
   *   An array of arguments to pass to the backend implementation.
   *
   * @return mixed
   *   The result of the forwarded method call.
   */
  public function __call(string $method, array $args) {
    if (!$this->backend instanceof BackendInterface) {
      throw new \RuntimeException('Backend unavailable');
    }

    if (!method_exists($this->backend, $method)) {
      throw new \BadMethodCallException(sprintf('Call to undefined method %s::%s()', get_class($this->backend), $method));
    }

    $callable = [
      $this->backend,
      $method,
    ];

    return call_user_func_array($callable, $args);
  }

}
