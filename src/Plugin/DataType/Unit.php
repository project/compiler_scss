<?php

namespace Drupal\compiler_scss\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\StringData;

/**
 * A data type used to represent a Sass unit.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @DataType(
 *   id = "compiler_scss_unit",
 *   label = @Translation("Sass unit"),
 *   constraints = {
 *     "Regex" = {
 *       "pattern" = "/^(%|ch|cm|deg|dpcm|dpi|dppx|em|ex|grad|Hz|in|kHz|lh|mm|ms|pc|pt|px|rad|rem|s|turn|vh|vmax|vmin|vw)$/",
 *       "message" = @Translation("An invalid Sass unit was provided.")
 *     },
 *   },
 * )
 */
class Unit extends StringData {

}
