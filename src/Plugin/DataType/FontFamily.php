<?php

namespace Drupal\compiler_scss\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\ItemList;

/**
 * A data type used to represent a font family sequence.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @DataType(
 *   id = "compiler_scss_font_family",
 *   label = @Translation("Font family"),
 *   definition_class = "\Drupal\compiler_scss\Config\Schema\FontFamilyDataDefinition",
 * )
 */
class FontFamily extends ItemList {

}
