<?php

namespace Drupal\compiler_scss\Plugin\DataType;

use Drupal\compiler_scss\Type\Color as IntermediateColor;
use Drupal\Core\TypedData\Plugin\DataType\StringData;

/**
 * A data type used to represent color values in "#RRGGBB[AA]" format.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @DataType(
 *   id = "compiler_scss_color",
 *   label = @Translation("Hexadecimal color"),
 *   constraints = {
 *     "Regex" = {
 *       "pattern" = "/^#((?P<hex>[A-F0-9]){3,4}|(?&hex){6}|(?&hex){8})$/i",
 *       "message" = @Translation("An invalid color value was provided.")
 *     },
 *   },
 * )
 */
class Color extends StringData {

  /**
   * {@inheritdoc}
   */
  public function getCastedValue() {
    return $this->getString();
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    try {
      return IntermediateColor::fromHex($this->value);
    }
    catch (\Throwable $e) {
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value, $notify = TRUE) {
    if ($value instanceof IntermediateColor) {
      $value = $value->toHex();
    }

    parent::setValue($value, $notify);
  }

}
