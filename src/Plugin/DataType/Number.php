<?php

namespace Drupal\compiler_scss\Plugin\DataType;

use Drupal\compiler_scss\Type\Number as IntermediateNumber;
use Drupal\Core\TypedData\Plugin\DataType\Map;

/**
 * A data type used to represent a Sass number.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @DataType(
 *   id = "compiler_scss_number",
 *   label = @Translation("Sass number"),
 *   definition_class = "\Drupal\compiler_scss\Config\Schema\NumberDataDefinition",
 * )
 */
class Number extends Map {

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    $value = $this->toArray() + [
      'value' => NULL,
      'unit' => NULL,
    ];

    if (is_numeric($value['value'])) {
      if (is_string($value['unit']) && strlen($value['unit']) > 0) {
        return new IntermediateNumber($value['value'], $value['unit']);
      }

      return floatval($value['value']);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value, $notify = TRUE) {
    if ($value instanceof IntermediateNumber) {
      $value = [
        'value' => $value->value(),
        'unit' => $value->unit(),
      ];
    }
    elseif (is_numeric($value)) {
      $value = [
        'value' => $value,
      ];
    }

    parent::setValue($value, $notify);
  }

}
