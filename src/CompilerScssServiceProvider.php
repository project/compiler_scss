<?php

namespace Drupal\compiler_scss;

use Drupal\compiler_scss\Backend\ScssPhp;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Registers the appropriate compiler backend service.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @internal
 */
class CompilerScssServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    // For now, the only supported backend is scssphp.
    $container->register('compiler_scss.backend', ScssPhp::class);
  }

}
