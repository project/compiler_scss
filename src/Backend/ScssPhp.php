<?php

namespace Drupal\compiler_scss\Backend;

use Drupal\compiler\CompilerContextInterface;
use Drupal\compiler_scss\BackendBase;
use Drupal\compiler_scss\Type\Color as IntermediateColor;
use Drupal\compiler_scss\Type\Number as IntermediateNumber;
use ScssPhp\ScssPhp\Compiler;
use ScssPhp\ScssPhp\Node\Number;
use ScssPhp\ScssPhp\Type;

/**
 * Provides a backend implementation bridge to scssphp.
 *
 * Copyright (C) 2021  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @internal
 */
class ScssPhp extends BackendBase {

  /**
   * {@inheritdoc}
   */
  public function compile(CompilerContextInterface $context) {
    $compiler = new Compiler();
    $compiler->setImportPaths([$this->getImportPath($context)]);

    // Iterate over each function descriptor for processing.
    foreach ($this->generateFunctionDescriptors() as $name => $descriptor) {
      // Register the function callback with the underlying compiler.
      $callback = $this->getCallbackFunction($descriptor);
      $compiler->registerFunction($name, $callback, $descriptor->params);
    }

    return $compiler->compileString($this->getInput($context))->getCss();
  }

  /**
   * {@inheritdoc}
   */
  protected function generateFunctionDescriptors(): \Generator {
    yield from $this->functions;
  }

  /**
   * {@inheritdoc}
   */
  public function processArgumentValue($value) {
    if (is_array($value) && array_key_exists(0, $value)) {
      $value = $this->processArgumentToken($value);
    }
    elseif ($value instanceof Number) {
      $value = $value->output();
      if (preg_match('/\D+$/', $value) !== 1) {
        $value = floatval($value);
      }
    }
    else {
      throw new \UnexpectedValueException('Unknown argument type');
    }

    return $value;
  }

  /**
   * Process an argument token supplied by the compiler.
   *
   * This method is responsible for the dynamic type juggling between the two
   * languages to improve the developer experience when writing functions.
   *
   * @param array $value
   *   A token from the underlying compiler.
   *
   * @return mixed
   *   The processed token.
   */
  protected function processArgumentToken(array $value) {
    switch ($value[0]) {
      case Type::T_NULL:
        $value = NULL;
        break;

      case Type::T_KEYWORD:
        // By default, assume we're dealing with an unquoted string.
        $value = $value[1] ?? '';

        try {
          // First, try to resolve the color by name.
          $value = IntermediateColor::fromName($value)->toHex();
        }
        catch (\UnexpectedValueException $e) {
          try {
            // Next, try to resolve the color by hex code.
            $value = IntermediateColor::fromHex($value)->toHex();
          }
          catch (\InvalidArgumentException $e) {
            // Finally, try to resolve true/false keywords.
            if (is_numeric($boolval = array_search(strtolower($value), ['false', 'true'], TRUE))) {
              $value = boolval($boolval);
            }
          }
        }

        break;

      case Type::T_STRING:
        $value = $value[2][0] ?? '';
        break;

      case Type::T_MAP:
        $keys = array_map([$this, 'processArgumentValue'], $value[1] ?? []);
        $invalid_keys = array_filter($keys, function ($key) {
          return !is_string($key) && !is_int($key);
        });

        if (count($invalid_keys) > 0) {
          throw new \UnexpectedValueException('Map argument tokens may only contain scalar keys');
        }
      case Type::T_LIST:
        $values = array_map([$this, 'processArgumentValue'], $value[2] ?? []);
        if (!isset($keys)) {
          $keys = count($values) > 0 ? range(0, count($values) - 1) : [];
        }

        $value = array_combine($keys, $values);
        break;

      case Type::T_COLOR:
        $value = (new IntermediateColor(
          $value[1] ?? 0,
          $value[2] ?? 0,
          $value[3] ?? 0,
          $value[4] ?? 1,
        ))->toHex();
        break;

      default:
        throw new \UnexpectedValueException('Unknown argument token type');
    }

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function processReturnValue($value) {
    switch (gettype($value)) {
      case 'NULL':
        $value = [Type::T_NULL];
        break;

      case 'integer':
      case 'double':
        $value = new Number($value, '');
        break;

      case 'boolean':
        $value = [Type::T_KEYWORD, boolval($value) ? 'true' : 'false'];
        break;

      case 'string':
        $value = [Type::T_STRING, '"', [$value]];
        break;

      case 'array':
        $value = array_map([$this, 'processReturnValue'], $value);
        if (count($value) === 0 || array_keys($value) === range(0, count($value) - 1)) {
          $value = [Type::T_LIST, ',', array_values($value)];
        }
        else {
          $keys = array_map([$this, 'processReturnValue'], array_keys($value));
          $value = [Type::T_MAP, $keys, array_values($value)];
        }

        break;

      case 'object':
        if ($value instanceof IntermediateColor) {
          $value = [
            Type::T_COLOR,
            $value->r(),
            $value->g(),
            $value->b(),
            $value->a(),
          ];

          break;
        }
        elseif ($value instanceof IntermediateNumber) {
          $value = new Number($value->value(), $value->unit());
          break;
        }
        elseif ($value instanceof \stdClass) {
          $value = $this->processReturnValue((array) $value);
          break;
        }

      default:
        throw new \UnexpectedValueException('Unsupported return type');
    }

    return $value;
  }

}
